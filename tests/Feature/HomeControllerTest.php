<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Sector;
use App\Models\UserSector;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_stores_form_data_to_a_session()
    {
        $this->post(route('home.store'), [
            'name' => 'John',
            'sectors' => [$id = Sector::factory()->create()->id],
            'terms' => 'on',
        ]);

        tap(UserSector::first(), function ($sector) use ($id) {
            self::assertEquals('John', $sector->name);
            self::assertEquals([$id], $sector->sectors);
            self::assertTrue($sector->terms);

            self::assertEquals($sector->id, session('sector'));
        });
    }

    /** @test */
    public function it_updates_existing_user_session()
    {
        $sectors = Sector::factory(2)->create();

        $this->post(route('home.store'), [
            'name' => 'John',
            'sectors' => [$sectors[0]->id],
            'terms' => 'on',
        ]);

        $this->post(route('home.store'), [
            'name' => 'Doe',
            'sectors' => [$id = $sectors[1]->id],
            'terms' => 'on'
        ]);

        tap(UserSector::first(), function ($sector) use ($id) {
           self::assertEquals('Doe', $sector->name);
           self::assertEquals([$id], $sector->sectors);
           self::assertTrue($sector->terms);
        });
    }
}
