<?php

namespace App\Http\Controllers;

use App\Models\Sector;
use App\Models\UserSector;

class HomeController extends Controller
{
    public function index()
    {
        $sectors = Sector::all();

        $session = UserSector::firstOrNew(['id' => session('sector')]);

        return view('home', compact('sectors', 'session'));
    }

    public function store()
    {
        $data = request()->validate([
            'name' => 'required|string|min:2',
            'sectors' => 'required|array|min:1|exists:sectors,id',
            'terms' => 'accepted'
        ]);

        if ($id = session('sector')) {
            UserSector::updateOrCreate(['id' => $id], $data);
        } else {
            session(['sector' => UserSector::create($data)->id]);
        }

        return redirect()->route('home')->with('status', 'Session Saved!');
    }
}
