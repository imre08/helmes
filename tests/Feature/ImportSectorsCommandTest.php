<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Sector;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ImportSectorsCommandTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_imports_sectors_from_html_file_to_the_database()
    {
        $this->artisan('import-sectors')->assertExitCode(0);

        self::assertEquals(79, Sector::count());

        self::assertEquals(7, Sector::where('name', 'Food and Beverage')->first()->children()->count());
    }
}
