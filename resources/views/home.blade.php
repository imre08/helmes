<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sectors - Helmes</title>

        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    </head>
    <body>
        <div id="root" class="mx-auto w-full px-2" style="max-width: 720px;">

            <div class="text-2xl mt-5">
                <h1>Sectors</h1>
            </div>

            @if (session('status'))
                <div class="bg-green-600 text-white p-2 mt-5 rounded">
                    {{ session('status') }}
                </div>
            @endif

            <p class="mt-5">Please enter your name and pick the Sectors you are currently involved in.</p>

            <form action="{{ route('home.store') }}" method="POST" class="mt-5">
                @csrf
                <div>
                    <label class="font-bold" for="name">Name</label>
                    <input class="border border-black p-1" type="text" id="name" name="name" value="{{ old('name', $session->name) }}" required>
                    @error('name')
                        <div class="text-red-500 py-2 text-sm">{{ $message }}</div>
                    @enderror
                </div>
                <div class="flex flex-col mt-5">
                    <label class="mb-2 font-bold" for="sectors">Sectors</label>
                    <select class="border border-black" name="sectors[]" id="sectors" multiple size="15" required>
                        @foreach($sectors as $sector)
                            <option
                                value="{{ $sector->id }}"
                                {{ in_array($sector->id, old('sectors', (array) $session->sectors)) ? 'selected' : '' }}
                            >
                                {!! str_repeat('&nbsp;', 4 * $sector->depth) !!} {{ $sector->name }}
                            </option>
                        @endforeach
                    </select>
                    @error('sectors')
                        <div class="text-red-500 py-2 text-sm">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mt-5">
                    <input type="checkbox" name="terms" id="terms" value="1" {{ old('terms', $session->terms) ? 'checked' : '' }}>
                    <label class="font-bold" for="terms">Agree to terms</label>
                    @error('terms')
                        <div class="text-red-500 py-2 text-sm">{{ $message }}</div>
                    @enderror
                </div>
                <button class="rounded bg-blue-500 text-white py-2 px-6 mt-5 font-bold" type="submit">Save</button>
            </form>

        </div>
    </body>
</html>
