
## Requirements
- PHP 7.4 or higher
- MySQL or Sqlite
- Composer

## Install

- run `composer install`
- make a copy of .env.example file and rename it to .env
- configure your database connection in .env file. Prefixed with `DB_`. You can also skip configuring connection as use Sqlite. Set `DB_CONNECTION=sqlite` and remove the rest of database variables.
- run `php artisan key:generate`
- run `php artisan migrate`
- run `php artisan import-sectors` to import sectors to the database.
