<?php

namespace App\Console\Commands;

use App\Models\Sector;
use Illuminate\Console\Command;

class ImportSectorsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import-sectors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import sectors from html file to the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->createSectors(
            $this->getTags()
        );

        $this->info('Completed!');
    }

    private function getTags() : \DOMNodeList
    {
        $document = new \DOMDocument;

        $file = storage_path('app/index.html');

        $document->loadHTMLFile($file);

        return $document->getElementsByTagName('option');
    }

    private function createSectors(\DOMNodeList $tags)
    {
        $parent = [];

        foreach ($tags as $tag) {
            $content = htmlentities($tag->textContent);

            $level = substr_count($content, '&nbsp;');

            $name = html_entity_decode(str_replace('&nbsp;', '', $content));

            $parent[$level] = Sector::create([
                'name' => trim($name),
                'depth' => $level / 4,
                'parent_id' => $parent[$level - 4] ?? null
            ])->id;
        }
    }
}
