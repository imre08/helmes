<?php

namespace App\Models;

class UserSector extends Model
{
    protected $guarded = [];

    protected $casts = [
        'sectors' => 'array',
        'terms' => 'boolean'
    ];
}
